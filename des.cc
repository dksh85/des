#include "des.h"

using namespace std;

Des :: Des(){
}

ui64 Des :: keyGen(ui64 key){
	ui64 permuted_choice_1 = 0; //56bits
	for(ui8 i = 0; i < 56; i++){
		permuted_choice_1 <<= 1;
		permuted_choice_1 |= (key >> (64-PC1[i])) & LB32_MASK;
	}
	cout << permuted_choice_1 << endl;
	return permuted_choice_1;
}

int main(){
	ui64 K = 1383827165325090801;
	Des des;
	des.keyGen(K);
}

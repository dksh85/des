#ifndef DES_H
#define DES_H

#include <iostream>
#include <stdint.h>

//#define ui8 uint8_t
typedef uint8_t ui8;
typedef uint32_t ui32;
typedef uint64_t ui64;
const ui64 LB32_MASK=0x00000001;

static const char PC1[] =
{
	57, 49, 41, 33, 25, 17,  9,
	1, 58, 50, 42, 34, 26, 18,
	10,  2, 59, 51, 43, 35, 27,
	19, 11,  3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14,  6, 61, 53, 45, 37, 29,
	21, 13,  5, 28, 20, 12,  4
};

class Des {
	public:
		Des();
		ui8 encrypt(char* msg);
		ui64 keyGen(ui64 key);
};

#endif
